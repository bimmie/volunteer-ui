module Route exposing (Route(..), fromUrl)

import Url exposing (Url)
import Url.Parser as UrlParser


type Route
    = Home
    | Event String
    | CreateEvent


fromUrl : Url -> Maybe Route
fromUrl url =
    case String.split "/" url.path of
        [ "" ] ->
            Just Home

        [ "", "" ] ->
            Just Home

        [ "", "create" ] ->
            Just CreateEvent

        [ "", eventid ] ->
            Just (Event eventid)

        _ ->
            Nothing
